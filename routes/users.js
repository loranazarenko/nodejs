var express = require('express');
var router = express.Router();

const { saveName } = require("../services/user.service");
const { getAllName } = require("../services/user.service");
const { getIdName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");
const { saveDataUser } = require("../services/user.service");
const { updData } = require("../services/user.service");
const { delDataUser } = require("../services/user.service");

router.get('/user', function(req, res, next) {
  //res.send(`Welcome!`);
  const result = getAllName();
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.get('/user/:id', function(req, res, next) {
  const result = getIdName(req.params.id);
  if (result) {
    console.log(result);
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.post('/', isAuthorized, function(req, res, next) {
  const result = saveName(req.body);

  if (result) {
    res.send(`Your name is ${result}`);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.post('/user', isAuthorized, function(req, res, next) {
  const result = saveDataUser(req.body);

  if (result) {
    res.send(`New user is ${result}`);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.put('/user/:id', isAuthorized, function(req, res, next) {
  const result = updData(req.body, req.params.id);

  if (result) {
    res.send(`Upd user is ${result}`);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.delete('/user/:id', isAuthorized, function(req, res, next) {
  const result = delDataUser(req.params.id);

  if (result) {
    res.send(`Delete user is ${result}`);
  } else {
    res.status(400).send(`Some error`);
  }
});

module.exports = router;
