const { saveData } = require("../repositories/user.repository");  ///.default
const { getAllData } = require("../repositories/user.repository");  ///.default
const { getIdData } = require("../repositories/user.repository");  ///.default
const { saveDataUserPost } = require("../repositories/user.repository");  ///.default
const { updDataUser } = require("../repositories/user.repository");  ///.default
const { deleteDataUser } = require("../repositories/user.repository");  ///.default

const getName = (user) => {
  if (user) {
    return user.name;
  } else {
    return null;
  }
};

const saveName = (user) => {
  if (user) {
    return saveData(user.name);
  } else {
    return null;
  }
};

const saveDataUser = (user) => {
  if (user) {
    return saveDataUserPost(user);
  } else {
    return null;
  }
};

const updData = (data, _id) => {
  if (_id) {
    return updDataUser(data, _id);
  } else {
    return null;
  }
};

const delDataUser = (_id) => {
  if (_id) {
    return deleteDataUser(_id);
  } else {
    return null;
  }
};

const getAllName = () => {
    return getAllData();
};

const getIdName = (_id) => {
  if (_id) {
    return getIdData(_id);
  } else {
    return null;
  }
};


module.exports = {
  getName,
  saveName,
  getAllName,
  getIdName,
  saveDataUser,
  updData,
  delDataUser
};