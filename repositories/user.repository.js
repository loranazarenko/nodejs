var userlist = require('./userlist');

const saveData = (data) => {
  // код по сохранению данных в БД
  if (data) {
    console.log(`${data} is saved`);
    return true;
  } else {
    return false;
  }
}

const saveDataUserPost = (data) => {
  // код по сохранению данных в БД
  if (data) {
    userlist.push(data);
    console.log(`${data} is saved`);
    console.log(userlist);
    return true;
  } else {
    return false;
  }
}

const updDataUser = (data, _id) => {
  console.log(data);
  console.log(_id);
  var indId = 0;
  if (_id) {
     for (let currentVal of userlist) {
        ++indId;
        console.log(currentVal._id);
        if (currentVal._id === _id) {
          break;
        }
     }
  } else {
      return false;
  }

  var updated = userlist.splice(indId-1, 1, data);
  if(updated) {
    console.log(userlist);
    return updated;
  }
  else {
    return false;
  }
}

const deleteDataUser = (_id) => {
  // код по сохранению данных в БД
  var indId = 0;
  if (_id) {
     for (let currentVal of userlist) {
        indId++;
        console.log(currentVal._id);
        if (currentVal._id === _id) {
          break;
        }
     }
  } else {
      return false;
  }

  var removed = userlist.splice(indId-1, 1);
  if(removed) {
    console.log(removed);
    return removed;
  }
  else {
    return false;
  }

}

const getAllData = () => {
  if (userlist) {
    console.log(userlist);
    return userlist;
  } else {
    return false;
  }
}

const getIdData = (_id) => {
  if (_id) {
      for (let currentVal of userlist) {
        console.log(currentVal._id);
        if (currentVal._id === _id) {
          return currentVal.name;
        }
        else{
           return false;
        }
      }
  } else {
    return false;
  }
}

module.exports = {
  saveData,
  getAllData,
  getIdData,
  saveDataUserPost,
  updDataUser,
  deleteDataUser
};
/** 
export default {
  saveData,
  getAllData
};
*/